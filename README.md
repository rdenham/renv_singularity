# R +  Renv + Singularity

Using R and Renv in a container to help achieve reproducibility. In this case,
we just want to have a simmple application that is guarenteed to use the same
libraries you used when you created the application. 

You can build the container from a recipe in the future, and you'll still
get the same version of R and whatever libraries you originally specified.



## Possible Workflow

Develop your application in rserver and in a project. Initialise this with `renv::init()` like normal.

Use `renv::snapshot()` to get your lockfile set. Add and commit the lockfile and `renv/activate.R` files to the repo.

Write your `def file`, here we'll make use of a prepared rocker image. Write your `.gitlab-ci.yml` file.

When you go to run your application, it will find the paths if you are in your project directory, but not
if you are somewhere else. To make this easier to work, you can write a simple bash script as your runscript, 
like

```sh
%runscript
  #!/bin/sh 
  if [ $# -ne 2 ]; then
     echo "Please provide alpha and beta parameters"
     exit 1
  fi
  cd /opt/$CI_PROJECT_NAME
  Rscript rbeta_density.R $1 $2
```

That means you can run your application like:

```sh
denhamr@athena52:renv_singularity$ singularity run shub://athena200.dnr.qld.gov.au/rsc/rbeta_density:latest 0.5 0.25
    +--+---------+----------+----------+---------+----------+--+
    |                                                    ****  |
  2 +                                                   **     +
    |                                                  **      |
    |                                                  *       |
1.5 +                                                 *        +
    |                                                **        |
    |                                               **         |
    |                                              **          |
  1 +                                             **           +
    |                                            **            |
    |  *******                                 ***             |
    |  *     *****                       ******                |
0.5 +            *************************                     +
    +--+---------+----------+----------+---------+----------+--+
       0        0.2        0.4        0.6       0.8         1   
```


Since you have a runscript, this means that you can treat the sif file as your executable.

e.g.,

```
singularity pull shub://athena200.dnr.qld.gov.au/rsc/rbeta_density:latest
./rbeta_density_latest.sif 0.1 0.7
denhamr@athena52:renv_singularity$ ./rbeta_density_latest.sif 0.1 0.7
   +--+----------+----------+---------+----------+----------+--+
   |  *                                                        |
15 +  *                                                        +
   |  *                                                        |
   |   *                                                       |
   |   *                                                       |
10 +   *                                                       +
   |   *                                                       |
   |   *                                                       |
   |   *                                                       |
 5 +   **                                                      +
   |    *                                                      |
   |    ***                                                    |
 0 +      ***************************************************  +
   +--+----------+----------+---------+----------+----------+--+
      0         0.2        0.4       0.6        0.8         1   

```